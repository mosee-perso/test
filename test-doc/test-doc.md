# Tests de mise en forme Markdown

## Equations

This math is inline: $`a^2+b^2=c^2`$.

This math is on a separate line:

```math
a^2+b^2=c^2
```

## Essai d'ajout d'images

"plateau1.png est inclus correctement :

![Schéma du plateau 1](plateau 1.png)

Par contre, "plateau 2.png" pose problème :

![Schéma du plateau 2](plateau 2.png)

Visiblement, il faut éviter les espaces dans les noms de fichiers, sauf si on mentionne un texte de remplacement.
